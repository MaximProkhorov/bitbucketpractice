import lombok.Data;
import lombok.Getter;

import java.util.Date;

@Data
public class Person {
    private String name;
    private String surname;
    private String otchestvo;
    private int age;
    private Gender gender;
    private Date dateOfBirth;
}